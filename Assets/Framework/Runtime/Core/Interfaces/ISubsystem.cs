﻿/*
 * Base interface of all Subsystems.
 * All subsystem's interfaces must to derived from this interface.
 * Author: Ideen Molavi Nejad, ideenmolavi@gmail.com
 * Creation Date : 26-08-2017
 */

namespace Revy.Framework
{
    /// <summary>
    /// Base interface of all Subsystems.
    /// All subsystem's interfaces must to derived from this interface.
    /// </summary>
    public interface ISubsystem : IDIRegister, IDispose
    {
    }
}