﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Revy.Framework
{
    public interface IDispose
    {
        void Dispose();
    }
}