﻿//TODO start framework change
namespace Revy.Framework
{
    public interface IActiveable
    {
        bool IsActive { get; set; }
    }
}
//TODO end framework change