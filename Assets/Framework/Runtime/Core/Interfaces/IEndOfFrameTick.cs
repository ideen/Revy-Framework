namespace Revy.Framework
{
    public interface IEndOfFrameTick : IActiveable
    {
        void EndOfFrameTick();
    }
}
