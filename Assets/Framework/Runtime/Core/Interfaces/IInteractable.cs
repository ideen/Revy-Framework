﻿/**
 * Author: Mahdi Fada
 * CreationTime: 9 / 25 / 2017
 * Description: The base interface for Interactable objects this mean object could click.
 **/

public interface IInteractable
{
    /// <summary>
    /// Call when object select
    /// </summary>
    void OnInteract();
}
