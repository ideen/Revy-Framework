using UnityEngine;
namespace Revy.Framework
{
    public class StateSystem : GenericStateSystem<string>, ISubsystem, IStateSystem
    {
        #region Properties

        public System.Type ServiceType => typeof(IStateSystem);

        #endregion Properties

        #region Constructor & Destructor

        public StateSystem()
        {
            MFramework.Register(this);
        }

        ~StateSystem()
        {
            Debug.Log("StateSystem Destructor has been invoked");
            MFramework.UnRegister(this);
        }

        #endregion Constructor & Destructor

        #region Class Interface

        public string GetState(string context)
        {
            return GetState<string>(context);
        }

        public void SetState(string context, string contextState)
        {
            SetState<string>(context, contextState);
        }

        void IDispose.Dispose()
        {

        }

        #endregion Public Methods
    }
}