/*
* Description:
* Author: Hesam Sehat, sehathesam@gmail.com
* Creation Data: 2019-08-22
*/

using System;
using System.Threading.Tasks;
using UnityEngine;
using Revy.Framework;

public class PoolingObject : FComponent
{
    
    #region Properties

    public string PoolingName { get; set; }

    #endregion

}
