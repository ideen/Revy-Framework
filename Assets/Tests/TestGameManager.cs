﻿using Revy.Framework;
using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;
namespace MyNamespace
{
    public class TestGameManager : FComponent, IGameManager, ITestGameManager, IInitializable, ITick, IInjectable,
        IDIRegister
    {
        #region Fields

#pragma warning disable 649
        [CInject] private IEventSystem _eventSystem;
        [CInject] private ISaveSystem _fileSystem;
        [CInject] private IPoolingSystem _poolSystem;
        private PureClass _pureClass = new PureClass();

#pragma warning restore 649

        [CInject] private IStateSystem _stateSystem;
        #endregion Fields

        #region Properties

        public Type ServiceType => typeof(ITestGameManager);

        #endregion Properties

        #region Public Methods

        void IDispose.Dispose()
        {

        }

        #endregion Public Methods

        #region FComponent's Callbacks

        public bool HasInitialized { get; set; }

        public void Initialize()
        {
            //_pureClass = new PureClass();
            //Debug.Log("Test Game Manger init");
            //_eventSystem.BroadcastEvent("a");
        }

        public Task BeginPlay()
        {
            //SceneManager.LoadScene(1);
            //var newObject = new GameObject("NewObject");
            //newObject.transform.SetParent(MFramework.TransientGameObject.transform);
            //_poolSystem.SetItem(newObject.name, newObject, 5, isExtendable: true);
            //GameObject lastItem = null;
            //for (int i = 0; i < 10; i++)
            //{
            //    lastItem = _poolSystem.GetItem(newObject.name);
            //}

            //_eventSystem.ListenToEvent("i", _LogSomthing);

            //            _eventSystem.BroadcastEvent("i");


            //                            
            //            var sw1 = new Stopwatch();
            //            sw1.Start();
            //            MeasureA();
            //            sw1.Stop();
            //            CLog.Log($"Measure A takes {sw1.ElapsedMilliseconds} ms");
            //            
            //            
            //            var sw2 = new Stopwatch();
            //            sw2.Start(); 
            //            MeasureB();
            //            sw2.Stop();
            //            CLog.Log($"Measure B takes {sw2.ElapsedMilliseconds} ms");


            return Task.CompletedTask;
        }

        public void Tick()
        {
            if (Input.GetKeyUp(KeyCode.R))
            {
                _eventSystem = null;
                //MFramework.Dispose();
            }

            if (Input.GetKeyUp(KeyCode.W))
            {
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            //_eventSystem = null;
            //_poolSystem = null;
            //_fileSystem = null;
            //_stateSystem = null;
            //_emptySystem = null;
            _pureClass = null;
        }

        #endregion FComponent's Callbacks

        #region Helpers     

        private Task _LogSomthing()
        {
            Debug.Log("Something");
            return Task.CompletedTask;
        }

        private Task _OnB()
        {
            return Task.CompletedTask;
        }

        private static Task _OnC()
        {
            return Task.CompletedTask;
        }

        private void MeasureA()
        {
            int result = 0;
            for (int i = 0; i < 1000000; i++)
            {
                result += 1;
            }
        }

        private void MeasureB()
        {
            int result = 0;
            for (int i = 0; i < 1000000; i++)
            {
                try
                {
                    result += 1;
                }
                catch (Exception)
                {
                }
            }
        }

        #endregion Helpers

        public bool IsActive { get; set; }
    }

    public class PureClass : IInjectable
    {
        public PureClass()
        {
            MFramework.Register(this);
        }

        ~PureClass()
        {
            Debug.Log("Pure Class Destructor has been invoked");
            MFramework.UnRegister(this);
        }
    }
}